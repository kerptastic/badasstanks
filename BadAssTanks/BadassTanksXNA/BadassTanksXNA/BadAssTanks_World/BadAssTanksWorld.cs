﻿using System.Collections.Generic;
using BadassTanksXNA.BadAssTanks_Objects;
using KerpEngine.Core;
using KerpEngine.Engine_2D;
using KerpEngine.Engine_2D.Sprites;
using KerpEngine.Engine_3D;
using KerpEngine.Engine_3D.Models;
using KerpEngine.Global;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace BadassTanksXNA.BadAssTanks_World
{
    /// <summary>
    /// 
    /// </summary>
    public class BadAssTanksWorld : GameWorld<GameObject2D, AABB>
    {
        private List<GameObject> _objects = new List<GameObject>();

        private float _totalGameTimePassed = 0.0f;

        private HeightMapHandler _heightMapHandler = new HeightMapHandler();

        //private GameObject2D obj1;
        //public GameObject2D TestObject { get { return obj1; } }

        private GameObject2D debugStats;
        public GameObject2D DebugStats { get { return debugStats; } }

        private GameObject3D cube;
        public GameObject3D Cube { get { return cube; } }

        private TestTerrain terrain;
        public TestTerrain Terrain { get { return terrain; } }

        public BadAssTanksWorld(TextureHandler textureHandler, ModelHandler modelHandler, Viewport viewport)
            : base(textureHandler, modelHandler, viewport)
        {
            this.BuildWorld(); 
            
            debugStats = new TextObject2D(new TextSprite2D(_textureHandler.GetFontTexture("courierNew"), "Hello World"), 220, 50, Color.White);

            
            //obj1 = new TitleTextObject2D(new UnanimatedSprite(_textureHandler.GetTexture("bad"), 1, 1), -10, -10, Color.White);
        
            cube = new TankObject3D(new CustomModel(_modelHandler.GetModel("test")), new Vector3(10.0f, 10.0f, 10.0f), new Vector3(0.0f, 0.0f, -1.0f), Color.White);
            //cube.ScaleValue = new Vector3(0.5f, 0.5f, 0.5f);

            //_spatialStructure.AddObject(obj1);
            _spatialStructure.AddObject(cube);

            _objects.Add(cube);
        }


        protected override void BuildWorld()
        {
            _spatialStructure = new Octree(new Vector3(0.0f, 0.0f, 0.0f), 500.0f);

            terrain = new TestTerrain(new Vector3(0, 0, 0));
            terrain.BuildTerrain("BadAssTanks_World\\terrain_2.txt");

        }


        public override void CheckCollisions()
        {
            
        }


        private void RepositionObjectInWorld(GameObject obj)
        {
            List<SpatialStructure> destNodes = _spatialStructure.FindNodeWhereObjectGoes(obj);
            List<SpatialStructure> actualNodes = obj.ContainedNodes;
            List<SpatialStructure> nodesToRemoveObjectFrom = new List<SpatialStructure>();

            for (int nodeCount = actualNodes.Count - 1; nodeCount >= 0; nodeCount--)
            {
                SpatialStructure node = actualNodes[nodeCount];

                if (destNodes.Contains(node))
                {
                    destNodes.Remove(node);
                }
                else
                {
                    nodesToRemoveObjectFrom.Add(node);
                }
            }

            foreach (SpatialStructure destNode in destNodes)
            {
                destNode.AddObject(obj);
            }

            foreach (SpatialStructure removeFromNode in nodesToRemoveObjectFrom)
            {
                removeFromNode.RemoveObject(obj);
            }
        }


        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            debugStats.Draw(spriteBatch);      
        }


        public override void Draw(GraphicsDevice device, BasicEffect effect, Matrix viewMatrix, Matrix projectionMatrix, GameTime gameTime)
        {
            _spatialStructure.Draw(device, effect);
            
            foreach (GameObject obj in _objects)
            {
                if (obj is GameObject2D)
                {
                    ((GameObject2D)obj).Draw(device, effect);
                }
                else
                {
                    ((GameObject3D)obj).Draw(device, effect, viewMatrix, projectionMatrix);
                }
            }

            terrain.Draw(device, effect, viewMatrix, projectionMatrix);
        }


        public override void Update(GameTime gameTime)
        {
            CheckCollisions();

            Random random = new Random();
            _totalGameTimePassed += gameTime.ElapsedGameTime.Milliseconds;

            //obj1.Update();
            debugStats.Update();
            //obj3.Update();
        }
    }
}
