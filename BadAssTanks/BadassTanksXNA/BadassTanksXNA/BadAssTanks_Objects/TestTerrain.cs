﻿using KerpEngine.Engine_3D;
using KerpEngine.Global;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BadassTanksXNA.BadAssTanks_Objects
{
    public class TestTerrain : GameObject3D
    {
        //private HeightMapHandler _heightMapHandler;
        //private VertexPositionColorNormal[] _vertices;
        //private int[] _indexBuffer;

        //private List<Triangle> _triangles;

        public TestTerrain(Vector3 position) : base(null, position, new Vector3(), Color.White)
        {
        //    _heightMapHandler = new HeightMapHandler();
        //    _indexBuffer = null;
        //    _triangles = new List<Triangle>();
        }

        public void BuildTerrain(string heightMapFile)
        {
            //int[][] heightMapData =
            //    _heightMapHandler.ReadIntBasedHeightMapFile(heightMapFile);

            //int width = heightMapData.Length;
            //int depth = heightMapData[0].Length;

            ////setup the vertices
            //_vertices = new VertexPositionColorNormal[width * depth];

            //for (int x = 0; x < width; x++)
            //{
            //    for (int z = 0; z < depth; z++)
            //    {
            //        _vertices[x + (z * width)].Position = new Vector3(x, heightMapData[x][z], -z);

            //        float heightColor = (float)heightMapData[x][z] * 5.0f / 255.0f;

            //        //if( z % 3 == 0)
            //        _vertices[x + (z * width)].Color = new Color(0.0f, heightColor, 0.0f);
            //        //else if (z % 3 == 1)
            //        //    _vertices[x + (z * width)].Color = Color.DarkBlue;
            //        //else
            //        //    _vertices[x + (z * width)].Color = Color.DarkGreen;
            //    }
            //}

            ////setup the indicies
            //_indexBuffer = new int[(width - 1) * (depth - 1) * 6];
            //int counter = 0;

            //for (int z = 0; z < depth - 1; z++)
            //{
            //    for (int x = 0; x < width - 1; x++)
            //    {
            //        int lowerLeft = x + z * width;
            //        int lowerRight = (x + 1) + z * width;
            //        int topLeft = x + (z + 1) * width;
            //        int topRight = (x + 1) + (z + 1) * width;

            //        _indexBuffer[counter++] = topLeft;
            //        _indexBuffer[counter++] = lowerRight;
            //        _indexBuffer[counter++] = lowerLeft;

            //        _indexBuffer[counter++] = topLeft;
            //        _indexBuffer[counter++] = topRight;
            //        _indexBuffer[counter++] = lowerRight;
            //    }
            //}

            ////setup the normals
            //for (int i = 0; i < _vertices.Length; i++)
            //{
            //    _vertices[i].Normal = new Vector3(0, 0, 0);
            //}

            //for (int i = 0; i < _indexBuffer.Length / 3; i++)
            //{
            //    int index1 = _indexBuffer[i * 3];
            //    int index2 = _indexBuffer[i * 3 + 1];
            //    int index3 = _indexBuffer[i * 3 + 2];

            //    Vector3 side1 = _vertices[index1].Position - _vertices[index3].Position;
            //    Vector3 side2 = _vertices[index1].Position - _vertices[index2].Position;
            //    Vector3 normal = Vector3.Cross(side1, side2);

            //    _vertices[index1].Normal += normal;
            //    _vertices[index2].Normal += normal;
            //    _vertices[index3].Normal += normal;
            //}

            //for (int i = 0; i < _vertices.Length; i++)
            //{
            //    _vertices[i].Normal.Normalize();
            //}
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="graphicsDevice">The Graphics Device to use to draw the Volume.</param>
        /// <param name="effect">The effect to use when drawing.</param>
        public override void Draw(GraphicsDevice graphicsDevice, BasicEffect effect, Matrix viewMatrix, Matrix projectionMatrix)
        {
            //Vector3 lightDirection = new Vector3(1.0f, -1.0f, -1.0f);
            //lightDirection.Normalize();

            //effect.DirectionalLight0.DiffuseColor = new Vector3(1.0f, 1.0f, 1.0f);
            //effect.DirectionalLight0.Direction = lightDirection;
            //effect.AmbientLightColor = new Vector3(0.2f, 0.2f, 0.2f);

            //Matrix worldMatrix = Matrix.Identity *
            //                     Matrix.CreateScale(_scale) *
            //                     Matrix.CreateRotationX(_rotation.X) *
            //                     Matrix.CreateRotationY(_rotation.Y) *
            //                     Matrix.CreateRotationZ(_rotation.Z) *
            //                     Matrix.CreateTranslation(_position);

            //effect.View = viewMatrix;
            //effect.Projection = projectionMatrix;
            //effect.World = worldMatrix;


            //foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            //{
            //    pass.Apply();

            //    graphicsDevice.DrawUserIndexedPrimitives(
            //        PrimitiveType.TriangleList,
            //        _vertices, 0, _vertices.Length, 
            //        _indexBuffer, 0, _indexBuffer.Length / 3, 
            //        VertexPositionColorNormal.VertexDeclaration);
            //}
        }
    }
}
