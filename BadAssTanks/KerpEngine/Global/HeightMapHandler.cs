﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace KerpEngine.Global
{
    public class HeightMapHandler
    {
        public HeightMapHandler()
        {

        }

        public int[][] ReadIntBasedHeightMapFile(String filepath)
        {
            int[][] heightMapData = null;
            
            using (StreamReader reader = File.OpenText(filepath))
            {
                string s = "";
                int width = 0, depth = 0, currentX = 0;
                bool rowZero = true;

                while ((s = reader.ReadLine()) != null)
                {
                    if (rowZero)
                    {
                        string[] widthDepth = s.Split('x');
                        width = Int32.Parse(widthDepth[0]);
                        depth = Int32.Parse(widthDepth[1]);

                        heightMapData = new int[width][];

                        rowZero = false;
                    }
                    else
                    {
                        string[] heightValues = s.Split(',');

                        int[] heights = new int[heightValues.Length];

                        for (int zValue = 0; zValue < heightValues.Length; zValue++)
                        {
                            //heightMapData[currentX][zValue] = Int32.Parse(heightValues[zValue]);
                            heights[zValue] = Int32.Parse(heightValues[zValue]);
                        }

                        heightMapData[currentX] = heights;
                        currentX++;
                    }
                }
            }

            return heightMapData;
        }
    }
}
