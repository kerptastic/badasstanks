﻿using System.Collections.Generic;
using KerpEngine.Engine_2D.Sprites;
using Microsoft.Xna.Framework.Graphics;
using KerpEngine.Engine_3D;
using KerpEngine.Engine_3D.Models;
using Microsoft.Xna.Framework;

namespace KerpEngine.Global
{
    /// <summary>
    /// A Octee Spatial Structure used for 3 dimensional scenes for clipping, object organization
    /// and collision detection.
    /// </summary>
    public class Octree : SpatialStructure
    {
        /// <summary>
        /// Constants for the Octree division rules.
        /// </summary>
        private const int MAX_OBJECTS_IN_NODE = 1;
        private const int MAX_TREE_DEPTH = 5;
        private float _size;

        public Octree(Vector3 position, float size)
        {
            _objects = new List<GameObject>();
            _childNodes = null;
            _boundingVolume = new OBB(position, size);
            _size = size;
        }

        public Octree(Vector3 position, float size, int depth) : this(position, size)
        {
            _depth = depth;
        }

        public override int GetMaxObjectsInNode()
        {
            return MAX_OBJECTS_IN_NODE;
        }

        public override int GetMaxTreeDepth()
        {
            return MAX_TREE_DEPTH;
        }

        public override void Divide(int parentDepth)
        {
            _childNodes = new Octree[8];

            float x = _boundingVolume.X;
            float y = _boundingVolume.Y;
            float z = _boundingVolume.Z;

            float width = ((OBB)_boundingVolume).Width;
            float height = ((OBB)_boundingVolume).Height;
            float depth = ((OBB)_boundingVolume).Depth;

            //top left front node
            _childNodes[0] = new Octree(
                new Vector3(
                    x - (width / 4.0f),
                    y + (height / 4.0f),
                    z + (depth / 4.0f)
                ),
                _size / 2.0f,
                parentDepth + 1);

            //top right front node
            _childNodes[1] = new Octree(
                new Vector3(
                    x + (width / 4.0f),
                    y + (height / 4.0f),
                    z + (depth / 4.0f)
                ),
                _size / 2.0f,
                parentDepth + 1);

            //bottom left front node
            _childNodes[2] = new Octree(
                new Vector3(
                    x - (width / 4.0f),
                    y - (height / 4.0f),
                    z + (depth / 4.0f)
                ),
                _size / 2.0f,
                parentDepth + 1);

            //bottom right front node
            _childNodes[3] = new Octree(
                new Vector3(
                    x + (width / 4.0f),
                    y - (height / 4.0f),
                    z + (depth / 4.0f)
                ),
                _size / 2.0f,
                parentDepth + 1);


            //top left back node
            _childNodes[4] = new Octree(
                new Vector3(
                    x - (width / 4.0f),
                    y + (height / 4.0f),
                    z - (depth / 4.0f)
                ),
                _size / 2.0f,
                parentDepth + 1);

            //top right back node
            _childNodes[5] = new Octree(
                new Vector3(
                    x + (width / 4.0f),
                    y + (height / 4.0f),
                    z - (depth / 4.0f)
                ),
                _size / 2.0f,
                parentDepth + 1);

            //bottom left back node
            _childNodes[6] = new Octree(
                new Vector3(
                    x - (width / 4.0f),
                    y - (height / 4.0f),
                    z - (depth / 4.0f)
                ),
                _size / 2.0f,
                parentDepth + 1);

            //bottom right back node
            _childNodes[7] = new Octree(
                new Vector3(
                    x + (width / 4.0f),
                    y - (height / 4.0f),
                    z - (depth / 4.0f)
                ),
                _size / 2.0f,
                parentDepth + 1);
        }   

        /// <summary>
        /// Draws this Octree node on the screen.
        /// </summary>
        public override void Draw(GraphicsDevice device, BasicEffect effect)
        {
            if (_childNodes != null)
            {
                for (int i = 0; i < _childNodes.Length; i++)
                {
                    ((Octree)_childNodes[i]).Draw(device, effect);
                }
            }

            _boundingVolume.Draw(device, effect);
        }
    }
}