﻿using KerpEngine.Engine_2D.Sprites;
using KerpEngine.Global;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace KerpEngine.Engine_2D
{
    /// <summary>
    /// A Quad Tree Spatial Structure used for 2 dimensional scenes for clipping, object organization
    /// and collision detection.
    /// </summary>
    public class QuadTree : SpatialStructure
    {
        /// <summary>
        /// Constants for the Quad Tree division rules.
        /// </summary>
        private const int MAX_OBJECTS_IN_NODE = 1;
        private const int MAX_TREE_DEPTH = 5;
        
        /// <summary>
        /// Creates a new Quad Tree at the given X/Y location with the given width and height.
        /// </summary>
        /// <param name="xLocation">The x location where the Quad Tree is located.</param>
        /// <param name="yLocation">The y location where the Quad Tree is located.</param>
        /// <param name="width">The width of the Quad Tree.</param>
        /// <param name="height">The height of the Quad Tree.</param>
        public QuadTree(QuadTree parent, float xLocation, float yLocation, float width, float height)
        {
            _parent = parent;
            _objects = new List<GameObject>();
            _childNodes = null;
            _boundingVolume = new AABB(new Vector3(xLocation, yLocation, 0.0f), width, height);
        }

        /// <summary>
        /// Creates a new Quad Tree at the given X/Y location with the given width and height.
        /// </summary>
        /// <param name="xLocation">The x location where the Quad Tree is located.</param>
        /// <param name="yLocation">The y location where the Quad Tree is located.</param>
        /// <param name="width">The width of the Quad Tree.</param>
        /// <param name="height">The height of the Quad Tree.</param>
        /// <param name="depth">The depth that this Quad Tree node is at.</param>
        public QuadTree(QuadTree parent, float xLocation, float yLocation, float width, float height, int depth) : this(parent, xLocation, yLocation, width, height)
        {
            _depth = depth;
        }

        public override int GetMaxObjectsInNode()
        {
            return MAX_OBJECTS_IN_NODE;
        }

        public override int GetMaxTreeDepth()
        {
            return MAX_TREE_DEPTH;
        }

        /// <summary>
        /// Divides this quad into 4 smaller quads.
        /// </summary>
        public override void Divide(int parentDepth)
        {
            _childNodes = new QuadTree[4];

            float x = _boundingVolume.X;
            float y = _boundingVolume.Y;
            float width = ((AABB)_boundingVolume).Width;
            float height = ((AABB)_boundingVolume).Height;

            //top left node
            _childNodes[0] = new QuadTree(this,
                x - (width / 4.0f),
                y + (height / 4.0f),
                width / 2.0f,
                height / 2.0f,
                parentDepth + 1);

            //top right node
            _childNodes[1] = new QuadTree(this,
                x + (width / 4.0f),
                y + (height / 4.0f),
                width / 2.0f,
                height / 2.0f,
                parentDepth + 1);

            //bottom left node
            _childNodes[2] = new QuadTree(this,
                x - (width / 4.0f),
                y - (height / 4.0f),
                width / 2.0f,
                height / 2.0f,
                parentDepth + 1);

            //bottom right node
            _childNodes[3] = new QuadTree(this,
                x + (width / 4.0f),
                y - (height / 4.0f),
                width / 2.0f,
                height / 2.0f,
                parentDepth + 1);
        }

        /// <summary>
        /// Draws this Quad Tree node on the screen.
        /// </summary>
        /// <param name="graphicsDevice">The Graphics Device to use to draw the Volume.</param>
        /// <param name="effect">The effect to use when drawing.</param>
        public override void Draw(GraphicsDevice graphicsDevice, BasicEffect effect)
        {
            if (_childNodes != null)
            {
                for (int i = 0; i < _childNodes.Length; i++)
                {
                    ((QuadTree)_childNodes[i]).Draw(graphicsDevice, effect);
                }
            }

            _boundingVolume.Draw(graphicsDevice, effect);
        }
    }
}