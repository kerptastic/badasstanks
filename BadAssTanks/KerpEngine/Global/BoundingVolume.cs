﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using KerpEngine.Engine_3D;
using System.Collections.Generic;

namespace KerpEngine.Global
{
    /// <summary>
    /// Interface defining the methods for a BoundingVolume.
    /// </summary>
    public abstract class BoundingVolume : GameObject3D
    {
        public enum IntersectionValue
        {
            CONTAINS, INTERSECTS, NONE
        }

        /// <summary>
        /// 
        /// </summary>
        protected List<Vector3> _normals = new List<Vector3>();
        public List<Vector3> Normals { get { return _normals; } }

        /// <summary>
        /// 
        /// </summary>
        protected List<Vector3> _collisionCorners = new List<Vector3>();
        public List<Vector3> CollisionCorners { get { return _collisionCorners; } }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="position"></param>
        /// <param name="target"></param>
        public BoundingVolume(Vector3 position, Vector3 target)
            : base(null, position, target, Color.White)
        {
        }

        /// <summary>
        /// Checks to see if the given Bounding Volume is intersecting this Bounding Volume.
        /// </summary>
        /// <param name="other">The Bounding Volume to see if is intersecting within this Bounding Volume.</param>
        /// <returns>Whether or not the given Bounding Volume is intersecting within this Bounding Volume.</returns>
        public abstract IntersectionValue Intersects(BoundingVolume other);

        /// <summary>
        /// Checks to see if the given Triangle is intersecting this Bounding Volume.
        /// </summary>
        /// <param name="other">The Triangle to see if is intersecting within this Bounding Volume.</param>
        /// <returns>Whether or not the given Triangle is intersecting within this Bounding Volume.</returns>
        public abstract IntersectionValue Intersects(Triangle other);

        /// <summary>
        /// Checks to see if the given Vector is contained in this Bounding Volume.
        /// </summary>
        /// <param name="other">The point to see if is contained within this Bounding Volume.</param>
        /// <returns>Whether or not the given point is contained within this Bounding Volume.</returns>
        public abstract bool Contains(Vector3 other);

        /// <summary>
        /// Draws this Bounding Volume to the screen.
        /// </summary>
        /// <param name="graphicsDevice">The Graphics Device to use to draw the Volume.</param>
        /// <param name="effect">The effect to use when drawing.</param>
        public abstract void Draw(GraphicsDevice graphicsDevice, BasicEffect effect);
    }
}
