﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace KerpEngine.Global
{
    /// <summary>
    /// Represents an abstract concept of a spatial strucuture, used to
    /// organize objects within a tree.
    /// </summary>
    /// <typeparam name="StoredObject_Type">The type of object to be stored within this structure.</typeparam>
    /// <typeparam name="BoundingShape_Type">The type of bounding volume to be used for this structure.</typeparam>
    public abstract class SpatialStructure
    {
        /// <summary>
        /// 
        /// </summary>
        protected SpatialStructure _parent;
        public SpatialStructure Parent { get { return _parent; } }
        /// <summary>
        /// The list of objects contained within this Structure's Node.
        /// </summary>
        protected List<GameObject> _objects;
        public List<GameObject> Objects { get { return _objects; } }
        /// <summary>
        /// The bounding shape around this structures node. Used for placement
        /// and drawing.
        /// </summary>
        protected BoundingVolume _boundingVolume;
        public BoundingVolume BoundingVolume { get { return _boundingVolume; } set { _boundingVolume = value; } }
        /// <summary>
        /// The child nodes of this Quad Tree Node.
        /// </summary>
        protected SpatialStructure[] _childNodes;
        public SpatialStructure[] ChildNodes { get { return _childNodes; } }


        protected int _depth = 0;

       
        /// <summary>
        /// Divides the structure into children nodes, moving the objects in the
        /// node to the child nodes.
        /// </summary>
        /// <param name="parentDepth">The depth down the tree of the parent node.</param>
        public abstract void Divide(int parentDepth);

        public abstract int GetMaxObjectsInNode();

        public abstract int GetMaxTreeDepth();

        /// <summary>
        /// Adds a game object to the structure.  If the structure has too many
        /// objects in it, and hasn't been divided already, it will divide
        /// itself and move its game objects into the children nodes.
        /// </summary>
        /// <param name="gameObj">The object to add to the structure.</param>
        public void AddObject(GameObject gameObj)
        {
            BoundingVolume.IntersectionValue containCheck =
                _boundingVolume.Intersects(gameObj.BoundingVolume);

            if (containCheck != BoundingVolume.IntersectionValue.NONE)
            {
                if (_childNodes == null)
                {
                    _objects.Add(gameObj);
                    gameObj.AddToContainedNodes(this);

                    if (_objects.Count > GetMaxObjectsInNode() && _depth < GetMaxTreeDepth())
                    {
                        this.Divide(this._depth);

                        for (int i = _objects.Count - 1; i >= 0; i--)
                        {
                            GameObject currentObj = _objects[i];

                            foreach (SpatialStructure childNode in _childNodes)
                            {
                                this.PlaceInChild(childNode, currentObj);
                            }

                            _objects.Remove(currentObj);
                            currentObj.RemoveFromContainedNodes(this);
                        }
                    }
                }
                else
                {
                    foreach (SpatialStructure childNode in _childNodes)
                    {
                        this.PlaceInChild(childNode, gameObj);
                    }
                }
            }
            else
            {
                _objects.Remove(gameObj);
                gameObj.RemoveFromContainedNodes(this);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        /// <param name="obj"></param>
        protected void PlaceInChild(SpatialStructure node, GameObject obj)
        {
            BoundingVolume.IntersectionValue result =
                node.BoundingVolume.Intersects(obj.BoundingVolume);

            if (result != BoundingVolume.IntersectionValue.NONE)
            {
                node.AddObject(obj);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gameObj"></param>
        /// <returns></returns>
        public List<SpatialStructure> FindNodeWhereObjectGoes(GameObject gameObj)
        {
            List<SpatialStructure> nodes = new List<SpatialStructure>();

            BoundingVolume.IntersectionValue containCheck = 
                _boundingVolume.Intersects(gameObj.BoundingVolume);

            if (containCheck != Global.BoundingVolume.IntersectionValue.NONE)
            {
                if (_childNodes != null)
                {
                    for (int i = 0; i < _childNodes.Length; i++)
                    {
                        nodes.AddRange(_childNodes[i].FindNodeWhereObjectGoes(gameObj));
                    }
                }
                else
                {
                    nodes.Add(this);
                }
            }

            return nodes;
        }


        /// <summary>
        /// Returns the Node that holds the given object.
        /// </summary>
        /// <param name="gameObj">The search object to find the containing node.</param>
        /// <returns>The Node that holds the given object.</returns>
        public List<SpatialStructure> FindNodeWhereObjectIs(GameObject gameObj)
        {
            List<SpatialStructure> nodes = new List<SpatialStructure>();

            if (_objects.Contains(gameObj))
            {
                nodes.Add(this);
            }

            if (_childNodes != null)
            {
                for (int i = 0; i < _childNodes.Length; i++)
                {
                    nodes.AddRange(_childNodes[i].FindNodeWhereObjectIs(gameObj));
                }
            }

            return nodes;
        }
         
        /// <summary>
        /// Removes an object from the entire structure, and prunes nodes if required.
        /// </summary>
        /// <param name="gameObj">The object to remove from the structure.</param>
        public void RemoveObject(GameObject gameObj)
        {
            _objects.Remove(gameObj);
            gameObj.RemoveFromContainedNodes(this);
        }
        
       
        /// <summary>
        /// 
        /// </summary>
        /// <param name="device"></param>
        /// <param name="effect"></param>
        public abstract void Draw(GraphicsDevice device, BasicEffect effect);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public HashSet<GameObject> GetAllObjects()
        {
            SpatialStructure topNode = this;

            while (topNode.Parent != null)
            {
                topNode = topNode.Parent;
            }

            return GetAllObjectsInNode(topNode);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public static HashSet<GameObject> GetAllObjectsInNode(SpatialStructure node)
        {
            HashSet<GameObject> allObjects = new HashSet<GameObject>();

            allObjects.UnionWith(node.Objects);

            if (node.ChildNodes != null)
            {
                for (int nodeCount = 0; nodeCount < node.ChildNodes.Length; nodeCount++)
                {
                    allObjects.UnionWith(GetAllObjectsInNode(node.ChildNodes[nodeCount]));
                }
            }

            return allObjects;
        }

        /// <summary>
        /// 
        /// </summary>
        public void RedistributeObjects()
        {
            HashSet<GameObject> myObjects = GetAllObjectsInNode(this);

            this._objects.Clear();

            //clear the children
            _childNodes = null;
           
            //add all of the objects back in
            foreach (GameObject gameObj in myObjects)
            {
                gameObj.ContainedNodes.Clear();

                this.AddObject(gameObj);
            }
        }
    }
}
