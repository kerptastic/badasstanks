﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KerpEngine.Global
{
    public class TriangleVertexIndices
    {
        /// <summary>
        /// 
        /// </summary>
        private uint _index1;
        public uint Index1 { get { return _index1; } }

        /// <summary>
        /// 
        /// </summary>
        private uint _index2;
        public uint Index2 { get { return _index2; } }
        
        /// <summary>
        /// 
        /// </summary>
        private uint _index3;
        public uint Index3 { get { return _index3; } }

        public TriangleVertexIndices(uint index1, uint index2, uint index3)
        {
            _index1 = index1;
            _index2 = index2;
            _index3 = index3;
        }
    }
}
