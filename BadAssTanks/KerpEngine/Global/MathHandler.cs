using System;
using Microsoft.Xna.Framework;

namespace KerpEngine.Global
{
    /// <summary>
    /// Class with static methods that handle odds and ends math routines.
    /// </summary>
    public class MathHandler
    {
        /// <summary>
        /// Returns the normal to a polygons surface, in which the polygon defined by three vectors.
        /// </summary>
        /// <param name="vert1">First vector of the polygon.</param>
        /// <param name="vert2">Second vector of the polygon.</param>
        /// <param name="vert3">Third vector of the polygon.</param>
        /// <returns></returns>
        public static Vector3 GetSurfaceNormal(Vector3 vert1, Vector3 vert2, Vector3 vert3)
        {
            //create the edges from the vertices
            Vector3 normal;

            normal = Vector3.Cross(vert1 - vert2, vert3 - vert2);
            normal.Normalize();

            return normal;
        }
    }
}
